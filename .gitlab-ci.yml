image: guergeiro/pnpm:16

stages:
  - build
  - deploy

before_script:
  - pnpm config set store-dir .pnpm-store

cache:
  key:
    files:
      - pnpm-lock.yaml
  paths:
    - .pnpm-store

lint:
  stage: build
  except:
    - main
  only:
    - branches
  dependencies: []
  allow_failure: true
  script:
    - pnpm install --loglevel=error
    - pnpm all:lint

test-helper:
  image: timbru31/node-chrome:latest
  stage: build
  except:
    - main
  only:
    - branches
  dependencies: []
  artifacts:
    expire_in: 1 week
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/dfx-helper/cobertura-coverage.xml
  coverage: '/Statements\s+:\s(\d+.?\d+)%/'
  allow_failure: true
  before_script:
    - curl -f https://get.pnpm.io/v6.16.js | node - add --global pnpm@7
    - pnpm config set store-dir .pnpm-store
  script:
    #    - wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
    #    - sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
    #    - apt-get update -qq && apt-get install -y -qq google-chrome-stable >> /dev/null
    #    - export CHROME_BIN=/usr/bin/google-chrome
    - pnpm install --loglevel=error
    - pnpm ngcc # Run npm-script ngcc, if ng build does it, the ci breaks sometimes (not enough memory)
    - pnpm helper:test:coverage

test-translate:
  image: timbru31/node-chrome:latest
  stage: build
  except:
    - main
  only:
    - branches
  dependencies: []
  artifacts:
    expire_in: 1 week
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/dfx-translate/cobertura-coverage.xml
  coverage: '/Statements\s+:\s(\d+.?\d+)%/'
  allow_failure: true
  before_script:
    - curl -f https://get.pnpm.io/v6.16.js | node - add --global pnpm@7
    - pnpm config set store-dir .pnpm-store
  script:
    - pnpm install --loglevel=error
    - pnpm ngcc # Run npm-script ngcc, if ng build does it, the ci breaks sometimes (not enough memory)
    - pnpm translate:test:coverage

build-dev:
  tags:
    - 'dafnik'
  stage: build
  only:
    - develop
  dependencies: []
  artifacts:
    expire_in: 1 week
    paths:
      - dist/
  script:
    - pnpm install --loglevel=error
    - pnpm ngcc # Run npm-script ngcc, if ng build does it, the ci breaks sometimes (not enough memory)
    - pnpm helper:build
    - pnpm translate:build
    - pnpm table:bs:build

deploy-dev:
  stage: deploy
  dependencies:
    - build-dev
  needs: ['build-dev']
  only:
    - develop
  cache: {}
  allow_failure: true
  script:
    - apt-get update -qq && apt-get install -y -qq zip git openssh-client >> /dev/null
    - eval "$(ssh-agent -s)"
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh && chmod 700 ~/.ssh
    - cd dist/
    - echo "Helper dev deployment"
    - zip -r "dfx-helper-dev.zip" ./dfx-helper >> /dev/null
    - scp -P $SSH_PORT -o StrictHostKeyChecking=no -o LogLevel=ERROR ./dfx-helper-dev.zip $SSH_USERNAME@$SSH_HOST:/var/www/datepoll-share/common/dfx-helper/ > /dev/null
    - echo "Tranlate dev deployment"
    - zip -r "dfx-translate-dev.zip" ./dfx-translate >> /dev/null
    - scp -P $SSH_PORT -o StrictHostKeyChecking=no -o LogLevel=ERROR ./dfx-translate-dev.zip $SSH_USERNAME@$SSH_HOST:/var/www/datepoll-share/common/dfx-translate/ > /dev/null
    - echo "Bootstrap table dev deployment"
    - zip -r "dfx-bootstrap-table-dev.zip" ./dfx-bootstrap-table >> /dev/null
    - scp -P $SSH_PORT -o StrictHostKeyChecking=no -o LogLevel=ERROR ./dfx-bootstrap-table-dev.zip $SSH_USERNAME@$SSH_HOST:/var/www/datepoll-share/common/dfx-bootstrap-table/ > /dev/null

build-helper:
  stage: build
  only:
    - /^helper-v.*$/
  except:
    - branches
    - triggers
  artifacts:
    expire_in: 1 week
    paths:
      - dist/
  script:
    - pnpm install --loglevel=error
    - pnpm ngcc # Run ngcc, if ng build does it, the ci breaks sometimes (not enough memory)
    - pnpm helper:build
    - cd dist/
    - ls -al

deploy-helper-release-zip:
  stage: deploy
  dependencies:
    - build-helper
  only:
    - /^helper-v.*$/
  except:
    - branches
    - triggers
  cache: {}
  allow_failure: true
  script:
    - apt-get update -qq && apt-get install -y -qq zip git openssh-client >> /dev/null
    - eval "$(ssh-agent -s)"
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh && chmod 700 ~/.ssh
    - version=$(node -p "require('./projects/dfx-helper/package.json').version")
    - cd dist/
    - zip -r "dfx-helper-v${version}.zip" ./dfx-helper >> /dev/null
    - cp "dfx-helper-v${version}.zip" dfx-helper-latest.zip
    - scp -P $SSH_PORT -o StrictHostKeyChecking=no -o LogLevel=ERROR ./dfx-helper-latest.zip $SSH_USERNAME@$SSH_HOST:/var/www/datepoll-share/common/dfx-helper/ > /dev/null
    - scp -P $SSH_PORT -o StrictHostKeyChecking=no -o LogLevel=ERROR "dfx-helper-v${version}.zip" $SSH_USERNAME@$SSH_HOST:/var/www/datepoll-share/common/dfx-helper/ > /dev/null

deploy-helper-release-npm:
  stage: deploy
  allow_failure: true
  dependencies:
    - build-helper
  only:
    - /^helper-v.*$/
  except:
    - branches
    - triggers
  cache: {}
  script:
    - cd dist/dfx-helper/
    - npm config set //registry.npmjs.org/:_authToken ${NPM_PUBLISH_TOKEN}
    - npm publish

build-translate:
  stage: build
  only:
    - /^translate-v.*$/
  except:
    - branches
    - triggers
  artifacts:
    expire_in: 1 week
    paths:
      - dist/
  script:
    - pnpm install --loglevel=error
    - pnpm ngcc # Run ngcc, if ng build does it, the ci breaks sometimes (not enough memory)
    - pnpm translate:build
    - cd dist/
    - ls -al

deploy-translate-release-zip:
  stage: deploy
  allow_failure: true
  dependencies:
    - build-translate
  only:
    - /^translate-v.*$/
  except:
    - branches
    - triggers
  cache: {}
  script:
    - apt-get update -qq && apt-get install -y -qq zip git openssh-client >> /dev/null
    - eval "$(ssh-agent -s)"
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
    - version=$(node -p "require('./projects/dfx-translate/package.json').version")
    - cd dist/
    - zip -r "dfx-translate-v${version}.zip" ./dfx-translate >> /dev/null
    - cp "dfx-translate-v${version}.zip" dfx-translate-latest.zip
    - scp -P $SSH_PORT -o StrictHostKeyChecking=no -o LogLevel=ERROR ./dfx-translate-latest.zip $SSH_USERNAME@$SSH_HOST:/var/www/datepoll-share/common/dfx-translate/ > /dev/null
    - scp -P $SSH_PORT -o StrictHostKeyChecking=no -o LogLevel=ERROR "dfx-translate-v${version}.zip" $SSH_USERNAME@$SSH_HOST:/var/www/datepoll-share/common/dfx-translate/ > /dev/null

deploy-translate-release-npm:
  stage: deploy
  allow_failure: true
  dependencies:
    - build-translate
  only:
    - /^translate-v.*$/
  except:
    - branches
    - triggers
  cache: {}
  script:
    - cd dist/dfx-translate/
    - npm config set //registry.npmjs.org/:_authToken ${NPM_PUBLISH_TOKEN}
    - npm publish

build-bootstrap-table-demo:
  stage: build
  only:
    - develop
  artifacts:
    expire_in: 2 hours
    paths:
      - dist/
  script:
    - pnpm install --loglevel=error
    - pnpm ngcc # Run npm-script ngcc, if ng build does it, the ci breaks sometimes (not enough memory)
    - pnpm table:bs:build
    - pnpm table:bs:demo:build
    - cd dist/
    - ls -al

deploy-bootstrap-table-demo:
  stage: deploy
  dependencies:
    - build-bootstrap-table-demo
  needs: ['build-bootstrap-table-demo']
  only:
    - develop
  cache: {}
  script:
    - apt-get update -qq && apt-get install -y -qq zip openssh-client >> /dev/null
    - eval "$(ssh-agent -s)"
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - echo "$SSH_TESTING_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh && chmod 700 ~/.ssh
    - cd dist/
    - zip -r dfx-bootstrap-table-demo.zip ./dfx-bootstrap-table-demo >> /dev/null
    - scp -P $SSH_PORT -o StrictHostKeyChecking=no -o LogLevel=ERROR ./dfx-bootstrap-table-demo.zip $SSH_USERNAME@$SSH_HOST:/var/www/datepoll-share/common/dfx-bootstrap-table/ > /dev/null
    - scp -P $SSH_TESTING_PORT -o StrictHostKeyChecking=no -rp ./dfx-bootstrap-table-demo/* $SSH_TESTING_USERNAME@$SSH_TESTING_HOST:/var/www/dfx-bootstrap-table-demo > /dev/null

build-bootstrap-table:
  stage: build
  only:
    - /^bs-table-v.*$/
  except:
    - branches
    - triggers
  artifacts:
    expire_in: 1 week
    paths:
      - dist/
  script:
    - pnpm install --loglevel=error
    - pnpm ngcc # Run ngcc, if ng build does it, the ci breaks sometimes (not enough memory)
    - pnpm table:bs:build
    - cd dist/
    - ls -al

deploy-bootstrap-table-release-zip:
  stage: deploy
  dependencies:
    - build-bootstrap-table
  only:
    - /^bs-table-v.*$/
  except:
    - branches
    - triggers
  cache: {}
  allow_failure: true
  script:
    - apt-get update -qq && apt-get install -y -qq zip git openssh-client >> /dev/null
    - eval "$(ssh-agent -s)"
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh && chmod 700 ~/.ssh
    - version=$(node -p "require('./projects/dfx-bootstrap-table/package.json').version")
    - cd dist/
    - zip -r "dfx-bootstrap-table-v${version}.zip" ./dfx-bootstrap-table >> /dev/null
    - cp "dfx-bootstrap-table-v${version}.zip" dfx-bootstrap-table-latest.zip
    - scp -P $SSH_PORT -o StrictHostKeyChecking=no -o LogLevel=ERROR ./dfx-bootstrap-table-latest.zip $SSH_USERNAME@$SSH_HOST:/var/www/datepoll-share/common/dfx-bootstrap-table/ > /dev/null
    - scp -P $SSH_PORT -o StrictHostKeyChecking=no -o LogLevel=ERROR "dfx-bootstrap-table-v${version}.zip" $SSH_USERNAME@$SSH_HOST:/var/www/datepoll-share/common/dfx-bootstrap-table/ > /dev/null

deploy-bootstrap-table-release-npm:
  stage: deploy
  dependencies:
    - build-bootstrap-table
  only:
    - /^bs-table-v.*$/
  except:
    - branches
    - triggers
  cache: {}
  script:
    - cd dist/dfx-bootstrap-table/
    - npm config set //registry.npmjs.org/:_authToken ${NPM_PUBLISH_TOKEN}
    - npm publish
