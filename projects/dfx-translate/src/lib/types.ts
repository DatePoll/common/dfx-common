export type translationFile = {[key: string]: string};
export type autoTranslationResponse = {translatedText: string};
