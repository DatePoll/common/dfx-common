/*
 * Public API Surface of dfx-translate
 */

export * from './lib/dfx-translate.module';
export * from './lib/translate.provider';
export * from './lib/translate.config';
export * from './lib/translate.service';
export * from './lib/translationKeys';
export * from './lib/types';

export * from './lib/pipes/tr';
export * from './lib/pipes/tra';
export * from './lib/pipes/trb';
