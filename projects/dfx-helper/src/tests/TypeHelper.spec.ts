import {isBoolean, isDate, isNumber, isNumeric, isString} from '../lib/helper/type';

describe('TypeHelper', () => {
  it('isBoolean', () => {
    expect(isBoolean('true')).toBeFalse();
    expect(isBoolean('false')).toBeFalse();
    expect(isDate(1)).toBeFalse();
    expect(isDate('test')).toBeFalse();
    expect(isDate(['test', 'test'])).toBeFalse();
    expect(isBoolean(true)).toBeTrue();
    expect(isBoolean(false)).toBeTrue();
    const test = true;
    const test1 = false;
    expect(isBoolean(test)).toBeTrue();
    expect(isBoolean(test1)).toBeTrue();
  });
  it('isDate', () => {
    expect(isDate('2020-12-12')).toBeFalse();
    expect(isDate('12:12')).toBeFalse();
    expect(isDate(1)).toBeFalse();
    expect(isDate('test')).toBeFalse();
    expect(isDate(true)).toBeFalse();
    expect(isDate(['test', 'test'])).toBeFalse();
    expect(isDate(new Date())).toBeTrue();
    expect(isDate(new Date('2020-12-12'))).toBeTrue();
    const test = new Date('2019-12-12');
    expect(isDate(test)).toBeTrue();
  });
  it('isNumeric', () => {
    expect(isNumeric('true')).toBeFalse();
    expect(isNumeric('false')).toBeFalse();
    expect(isNumeric('asdf0000')).toBeFalse();
    expect(isNumeric('001212asdfasd2342')).toBeTrue();
    expect(isNumeric('123')).toBeTrue();
    expect(isNumeric('-12')).toBeTrue();
    expect(isNumeric('0.00000002323')).toBeTrue();
    expect(isNumeric('121212.00000002323')).toBeTrue();
    expect(isNumeric('00000.0000')).toBeTrue();
    expect(isNumeric(2048)).toBeTrue();
    expect(isNumeric(true)).toBeFalse();
  });
  it('isString', () => {
    expect(isString('true')).toBeTrue();
    expect(isString('false')).toBeTrue();
    expect(isString('asdf0000')).toBeTrue();
    expect(isString('001212asdfasd2342')).toBeTrue();
    expect(isString('123')).toBeTrue();
    expect(isString('-12')).toBeTrue();
    expect(isString('0.00000002323')).toBeTrue();
    expect(isString('121212.00000002323')).toBeTrue();
    expect(isString('00000.0000')).toBeTrue();
  });
  it('isNumber', () => {
    expect(isNumber('true')).toBeFalse();
    expect(isNumber('false')).toBeFalse();
    expect(isNumber('asdf0000')).toBeFalse();
    expect(isNumber('001212asdfasd2342')).toBeFalse();
    expect(isNumber('123')).toBeFalse();
    expect(isNumber('-12')).toBeFalse();
    expect(isNumber('0.00000002323')).toBeFalse();
    expect(isNumber('121212.00000002323')).toBeFalse();
    expect(isNumber('00000.0000')).toBeFalse();
    expect(isNumber(0)).toBeTrue();
    expect(isNumber(1000.1)).toBeTrue();
    expect(isNumber(-1)).toBeTrue();
    expect(isNumber(Number.MAX_SAFE_INTEGER)).toBeTrue();
    expect(isNumber(Number.MAX_SAFE_INTEGER + 1)).toBeTrue();
  });
});
