import {Delay} from '../lib/decorators/delay';

describe('Delay', () => {
  it('should delay the execution of the decorated method by the specified number of milliseconds', (done) => {
    // keep track of the time when the test method is called
    const start = Date.now();

    // call the test method
    TestClass.testMethod(null);

    // expect the decorated method to be called after 1 second
    setTimeout(() => {
      const end = Date.now();
      expect(end - start).toBeGreaterThanOrEqual(1000);
      done();
    }, 1100);
  });

  it('should pass the arguments to the decorated method when it is called', (done) => {
    // mock the decorated method to spy on it
    spyOn(TestClass, 'testMethod').and.callThrough();

    // call the test method with some arguments
    TestClass.testMethod('hello');

    // expect the decorated method to be called with the correct arguments after 1 second
    setTimeout(() => {
      expect(TestClass.testMethod).toHaveBeenCalledWith('hello');
      done();
    }, 1100);
  });
});

class TestClass {
  @Delay(1000)
  static testMethod(value: any): any {
    return value;
  }

  @Delay(1000)
  static testThrow(value: any): any {
    throw new Error('test error');
  }
}
