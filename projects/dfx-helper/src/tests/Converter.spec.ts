import {nullToUndefined, toBoolean, toNumber, toString, undefinedToNull} from '../lib/helper/converter';

describe('Converter', () => {
  it('toNumber', () => {
    expect(toNumber(null)).toBe(Number.MAX_SAFE_INTEGER);
    expect(toNumber(undefined)).toBe(Number.MAX_SAFE_INTEGER);
    expect(toNumber(12)).toBe(12);

    expect(toNumber('1')).toBe(1);
    expect(toNumber('1')).toBeDefined();
    expect(toNumber('120000')).toBe(120000);
    expect(toNumber('123.12')).toBe(123.12);
    expect(toNumber('0.00000012')).toBe(0.00000012);

    expect(toNumber(true)).toBeDefined();
    expect(toNumber(true)).toBe(1);
    expect(toNumber(false)).toBe(0);
  });
  it('toString', () => {
    expect(toString(null)).toBe('');
    expect(toString(undefined)).toBe('');
    expect(toString('irgendwas')).toBe('irgendwas');
  });
  it('toBoolean', () => {
    expect(toBoolean(true)).toBeTrue();
    expect(toBoolean(false)).toBeFalse();

    expect(toBoolean('true')).toBeTrue();
    expect(toBoolean('true')).toBeDefined();
    expect(toBoolean('false')).toBeFalse();
    expect(toBoolean('asdfasdfasdf')).toBeFalse();
    expect(toBoolean('dddddd')).toBeFalse();
    expect(toBoolean(null)).toBeFalse();
    expect(toBoolean(undefined)).toBeFalse();

    expect(toBoolean(1)).toBeDefined();
    expect(toBoolean(1)).toBeTrue();
    expect(toBoolean(12)).toBeFalse();
    expect(toBoolean(0)).toBeFalse();
    expect(toBoolean(-12)).toBeFalse();
    expect(toBoolean(12.213)).toBeFalse();
    expect(toBoolean(-1)).toBeFalse();
  });
  it('toString', () => {
    expect(toString(true)).toBeDefined();
    expect(toString(true)).toBe('true');
    expect(toString(false)).toBe('false');

    expect(toString(1)).toBe('1');
    expect(toString(1)).toBeDefined();
    expect(toString(120000)).toBe('120000');
    expect(toString(123.12)).toBe('123.12');
    expect(toString(0.000012)).toBe('0.000012');
  });
});

describe('undefinedToNull function', () => {
  it('undefinedToNull', () => {
    let test: string | undefined = undefined;
    expect(undefinedToNull(test)).toBeNull();
  });
  it('should convert an undefined value to null', () => {
    const result = undefinedToNull(undefined);
    expect(result).toEqual(null);
  });

  it('should return the original value if it is not undefined', () => {
    const result = undefinedToNull('hello');
    expect(result).toEqual('hello');
  });
  it('should return null if the input value is null', () => {
    const result = undefinedToNull(null);
    expect(result).toEqual(null);
  });

  it('should work correctly when the input value is an object', () => {
    const obj = {foo: 'bar'};
    const result = undefinedToNull(obj);
    expect(result).toEqual(obj);
  });

  it('should work correctly when the input value is an array', () => {
    const arr = [1, 2, 3];
    const result = undefinedToNull(arr);
    expect(result).toEqual(arr);
  });
});

describe('nullToUndefined function', () => {
  it('nullToUndefined', () => {
    let test: string | null = null;
    expect(nullToUndefined(test)).toBeUndefined();
  });
  it('should convert a null value to undefined', () => {
    const result = nullToUndefined(null);
    expect(result).toEqual(undefined);
  });

  it('should return the original value if it is not null', () => {
    const result = nullToUndefined('hello');
    expect(result).toEqual('hello');
  });

  it('should return undefined if the input value is undefined', () => {
    const result = nullToUndefined(undefined);
    expect(result).toEqual(undefined);
  });

  it('should work correctly when the input value is an object', () => {
    const obj = {foo: 'bar'};
    const result = nullToUndefined(obj);
    expect(result).toEqual(obj);
  });

  it('should work correctly when the input value is an array', () => {
    const arr = [1, 2, 3];
    const result = nullToUndefined(arr);
    expect(result).toEqual(arr);
  });
});
