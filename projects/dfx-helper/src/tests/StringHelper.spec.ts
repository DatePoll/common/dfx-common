import {StringHelper} from '../lib/helper/string/_string';
import {imploderBuilder} from '../lib/helper/string/imploderBuilder';
import {isNoUrl, isUrl} from '../lib/helper/string/url';
import {isEmail} from '../lib/helper/string/email';
import {upperCaseFirstLetter} from '../lib/helper/string/transformers';
import {truncate} from '../lib/helper/string/truncate';

describe('StringHelper', () => {
  it('isUrl', () => {
    expect(isUrl('http://ab.at')).toBeTrue();
    expect(isUrl('https://datepoll.org')).toBeTrue();
    expect(isUrl('datepollsystems.org')).toBeTrue();
    expect(isUrl('datepollsystemsorg')).toBeFalse();
    expect(isUrl(':datepollsy.stemsorg')).toBeFalse();
    expect(isUrl('httpdatepollsy.stemsorg')).toBeTrue();
    expect(isUrl('httpsdatepollsy.stemsorg')).toBeTrue();
    expect(isUrl('httpsdatepollsy:stemsorg')).toBeFalse();
    expect(isUrl('httpdatepollsy:stemsorg')).toBeFalse();
    expect(isUrl('/httpdatepollsy.stemsorg')).toBeFalse();
    expect(isUrl('/httpsdatepollsy.stemsorg')).toBeFalse();
    expect(isUrl('/httpsdatepollsy:stemsorg')).toBeFalse();
    expect(isUrl('/httpdatepollsy:stemsorg')).toBeFalse();
    expect(isUrl('/httpdatepollsy.stemsorg:')).toBeFalse();
    expect(isUrl('/httpsdatepollsy.stemsorg:')).toBeFalse();
    expect(isUrl('/httpsdatepollsy:stemsorg:')).toBeFalse();
    expect(isUrl('/httpdatepollsy:stemsorg:')).toBeFalse();
    expect(isUrl('http://a.at')).toBeTrue();
  });
  it('isEmail', () => {
    expect(isEmail('test@test.at')).toBeTrue();
    expect(isEmail('keineemail')).toBeFalse();
    expect(isEmail('test.at')).toBeFalse();
    expect(isEmail('test@testt')).toBeFalse();
    expect(isEmail('test@.at')).toBeFalse();
  });
  it('isNoUrl', () => {
    expect(isNoUrl('http://ab.at')).toBeFalse();
    expect(isNoUrl('https://datepoll.org')).toBeFalse();
    expect(isNoUrl('datepollsystems.org')).toBeFalse();
    expect(isNoUrl('httpdatepollsy.stemsorg')).toBeFalse();
    expect(isNoUrl('httpsdatepollsy.stemsorg')).toBeFalse();
    expect(isNoUrl('http://a.at')).toBeFalse();
    expect(isNoUrl('datepollsystemsorg')).toBeTrue();
    expect(isNoUrl(':datepollsy.stemsorg')).toBeTrue();
    expect(isNoUrl('httpsdatepollsy:stemsorg')).toBeTrue();
    expect(isNoUrl('httpdatepollsy:stemsorg')).toBeTrue();
    expect(isNoUrl('/httpdatepollsy.stemsorg')).toBeTrue();
    expect(isNoUrl('/httpsdatepollsy.stemsorg')).toBeTrue();
    expect(isNoUrl('/httpsdatepollsy:stemsorg')).toBeTrue();
    expect(isNoUrl('/httpdatepollsy:stemsorg')).toBeTrue();
    expect(isNoUrl('/httpdatepollsy.stemsorg:')).toBeTrue();
    expect(isNoUrl('/httpsdatepollsy.stemsorg:')).toBeTrue();
    expect(isNoUrl('/httpsdatepollsy:stemsorg:')).toBeTrue();
    expect(isNoUrl('/httpdatepollsy:stemsorg:')).toBeTrue();
  });
  it('isNoEmail', () => {
    expect(StringHelper.isNoEmail('test@test.at')).toBeFalse();
    expect(StringHelper.isNoEmail('keineemail')).toBeTrue();
    expect(StringHelper.isNoEmail('test.at')).toBeTrue();
    expect(StringHelper.isNoEmail('test@testt')).toBeTrue();
    expect(StringHelper.isNoEmail('test@.at')).toBeTrue();
  });
  it('hasNumbersInString', () => {
    expect(StringHelper.hasNumbersInString('true')).toBeFalse();
    expect(StringHelper.hasNumbersInString('false')).toBeFalse();
    expect(
      StringHelper.hasNumbersInString('lajhsdlkfqwejröqjkafkjöaskdjfvövjaölwekjörqqwer!"§$!"§$%§$%&"$§"§!"§$&/%&/$(&/(=&/(%$&/§$')
    ).toBeFalse();
    expect(StringHelper.hasNumbersInString('asdf0000')).toBeTrue();
    expect(StringHelper.hasNumbersInString('001212asdfasd2342')).toBeTrue();
    expect(StringHelper.hasNumbersInString('123')).toBeTrue();
    expect(StringHelper.hasNumbersInString('-12')).toBeTrue();
    expect(StringHelper.hasNumbersInString('0.00000002323')).toBeTrue();
    expect(StringHelper.hasNumbersInString('121212.00000002323')).toBeTrue();
    expect(StringHelper.hasNumbersInString('00000.0000')).toBeTrue();
  });
  it('hasNoNumbersInString', () => {
    expect(StringHelper.hasNoNumbersInString('true')).toBeTrue();
    expect(StringHelper.hasNoNumbersInString('false')).toBeTrue();
    expect(
      StringHelper.hasNoNumbersInString('lajhsdlkfqwejröqjkafkjöaskdjfvövjaölwekjörqqwer!"§$!"§$%§$%&"$§"§!"§$&/%&/$(&/(=&/(%$&/§$')
    ).toBeTrue();
    expect(StringHelper.hasNoNumbersInString('asdf0000')).toBeFalse();
    expect(StringHelper.hasNoNumbersInString('001212asdfasd2342')).toBeFalse();
    expect(StringHelper.hasNoNumbersInString('123')).toBeFalse();
    expect(StringHelper.hasNoNumbersInString('-12')).toBeFalse();
    expect(StringHelper.hasNoNumbersInString('0.00000002323')).toBeFalse();
    expect(StringHelper.hasNoNumbersInString('121212.00000002323')).toBeFalse();
    expect(StringHelper.hasNoNumbersInString('00000.0000')).toBeFalse();
  });
  it('hasOnlyLettersInString', () => {
    expect(StringHelper.hasOnlyLettersInString('true')).toBeTrue();
    expect(StringHelper.hasOnlyLettersInString('false')).toBeTrue();
    expect(StringHelper.hasOnlyLettersInString('false.')).toBeFalse();
    expect(StringHelper.hasOnlyLettersInString('false-')).toBeFalse();
    expect(StringHelper.hasOnlyLettersInString('false+')).toBeFalse();
    expect(StringHelper.hasOnlyLettersInString('false#')).toBeFalse();
    expect(
      StringHelper.hasOnlyLettersInString('lajhsdlkfqwejröqjkafkjöaskdjfvövjaölwekjörqqwer!"§$!"§$%§$%&"$§"§!"§$&/%&/$(&/(=&/(%$&/§$--..')
    ).toBeFalse();
    expect(StringHelper.hasOnlyLettersInString('asdf0000')).toBeFalse();
    expect(StringHelper.hasOnlyLettersInString('001212asdfasd2342')).toBeFalse();
    expect(StringHelper.hasOnlyLettersInString('123')).toBeFalse();
    expect(StringHelper.hasOnlyLettersInString('-12')).toBeFalse();
    expect(StringHelper.hasOnlyLettersInString('0.00000002323')).toBeFalse();
    expect(StringHelper.hasOnlyLettersInString('121212.00000002323')).toBeFalse();
    expect(StringHelper.hasOnlyLettersInString('00000.0000')).toBeFalse();
  });
  it('hasNotOnlyLettersInString', () => {
    expect(StringHelper.hasNotOnlyLettersInString('true')).toBeFalse();
    expect(StringHelper.hasNotOnlyLettersInString('false')).toBeFalse();
    expect(StringHelper.hasNotOnlyLettersInString('false.')).toBeTrue();
    expect(StringHelper.hasNotOnlyLettersInString('false-')).toBeTrue();
    expect(StringHelper.hasNotOnlyLettersInString('false+')).toBeTrue();
    expect(StringHelper.hasNotOnlyLettersInString('false#')).toBeTrue();
    expect(
      StringHelper.hasNotOnlyLettersInString(
        'lajhsdlkfqwejröqjkafkjöaskdjfvövjaölwekjörqqwer!"§$!"§$%§$%&"$§"§!"§$&/%&/$(&/(=&/(%$&/§$--..'
      )
    ).toBeTrue();
    expect(StringHelper.hasNotOnlyLettersInString('asdf0000')).toBeTrue();
    expect(StringHelper.hasNotOnlyLettersInString('001212asdfasd2342')).toBeTrue();
    expect(StringHelper.hasNotOnlyLettersInString('123')).toBeTrue();
    expect(StringHelper.hasNotOnlyLettersInString('-12')).toBeTrue();
    expect(StringHelper.hasNotOnlyLettersInString('0.00000002323')).toBeTrue();
    expect(StringHelper.hasNotOnlyLettersInString('121212.00000002323')).toBeTrue();
    expect(StringHelper.hasNotOnlyLettersInString('00000.0000')).toBeTrue();
  });
  it('cutString', () => {
    expect(StringHelper.cut('DasIstEinTest', 5)).toBe('DasIs...');
    expect(StringHelper.cut('DasIstEinTest', 20)).toBe('DasIstEinTest');
    expect(StringHelper.cut('DasIstEinTest')).toBe('DasIstEinT...');
    expect(StringHelper.cut('DasIstEinTest', 5, null)).toBe('DasIs');
    expect(StringHelper.cut('DasIstEinTest', 5, '..')).toBe('DasIs..');
    expect(StringHelper.cut(null)).toBe('');
    expect(StringHelper.cut(undefined)).toBe('');
    expect(StringHelper.cut('hello', 10)).toBe('hello');
    expect(StringHelper.cut('goodbye', 20)).toBe('goodbye');
    expect(StringHelper.cut('hello world', 5)).toBe('hello...');
    expect(StringHelper.cut('the quick brown fox', 10)).toBe('the quick ...');
    expect(StringHelper.cut('hello world', 5, '!')).toBe('hello!');
    expect(StringHelper.cut('the quick brown fox', 10, '***')).toBe('the quick ***');
    expect(StringHelper.cut('hello world', 5, '')).toBe('hello');
    expect(StringHelper.cut('the quick brown fox', 10, '')).toBe('the quick ');
  });
  it('imploder', () => {
    const test = ['Apple', 'Bannana', 'Rasberry', 'Pie', 'Ananas', 'Lannanas'];
    expect(imploderBuilder(test).separator(', ').build()).toBe('Apple, Bannana, Rasberry, Pie, Ananas, Lannanas');
    expect(imploderBuilder(test).separator(', ').maxLength(40).suffix('...').build()).toBe('Apple, Bannana, Rasberry, Pie, Ananas, L...');
    expect(imploderBuilder(test).separator(', ').maxLength(40).build()).toBe('Apple, Bannana, Rasberry, Pie, Ananas, L');
    expect(imploderBuilder(test).separator(', ').maxLength(15).build()).toBe('Apple, Bannana,');
    expect(imploderBuilder(test).separator(', ').maxLength(15).suffix('BBB').build()).toBe('Apple, Bannana,BBB');
    expect(imploderBuilder(test).separator(', ').maxLength(15).suffix('...').build()).toBe('Apple, Bannana,...');
    expect(imploderBuilder(test).separator('; ').build()).toBe('Apple; Bannana; Rasberry; Pie; Ananas; Lannanas');
  });
  it('upperCaseFirstLetter', () => {
    expect(upperCaseFirstLetter(null)).toBe('');
    expect(upperCaseFirstLetter(undefined)).toBe('');
    expect(upperCaseFirstLetter('hello')).toBe('Hello');
    expect(upperCaseFirstLetter('goodbye')).toBe('Goodbye');
    expect(upperCaseFirstLetter('hello world')).toBe('Hello world');
    expect(upperCaseFirstLetter('the quick brown fox')).toBe('The quick brown fox');
    expect(upperCaseFirstLetter('1 hello')).toBe('1 hello');
    expect(upperCaseFirstLetter('$100')).toBe('$100');
    expect(upperCaseFirstLetter('hello world')).toBe('Hello world');
    expect(upperCaseFirstLetter('the quick brown fox')).toBe('The quick brown fox');
  });
  it('truncate', () => {
    expect(truncate(null)).toBe('');
    expect(truncate(undefined)).toBe('');
    expect(truncate('hello world', 10)).toBe('hello world');
    expect(truncate('the quick brown fox', 20)).toBe('the quick brown fox');
    expect(truncate('hello world', 1)).toBe('hello...');
    expect(truncate('the quick brown fox', 2)).toBe('the quick...');
    expect(truncate('hello world', 1, '!')).toBe('hello!');
    expect(truncate('the quick brown fox', 2, '***')).toBe('the quick***');
    expect(truncate('hello world', 1, '')).toBe('hello');
    expect(truncate('the quick brown fox', 2, '')).toBe('the quick');
  });
  it('toChunks', () => {
    expect(StringHelper.toChunks('', 10).length).toEqual(0);
    expect(StringHelper.toChunks('hello world', 5)).toEqual(['hello', 'world']);
    expect(StringHelper.toChunks('the quick brown fox', 10)).toEqual(['the quick', 'brown fox']);
    expect(StringHelper.toChunks('hello world', 100)).toEqual(['hello world']);
    expect(StringHelper.toChunks('the quick brown fox', 1000)).toEqual(['the quick brown fox']);
    expect(StringHelper.toChunks('    ', 10)).toEqual([]);
    expect(StringHelper.toChunks('\t\t\t\t', 5)).toEqual([]);
    expect(StringHelper.toChunks('  hello world  ', 5)).toEqual(['hello', 'world']);
    expect(StringHelper.toChunks('  the quick brown fox   ', 10)).toEqual(['the quick', 'brown fox']);
    expect(StringHelper.toChunks('hello   world', 5)).toEqual(['hello', 'world']);
    expect(StringHelper.toChunks('the   quick   brown   fox', 10)).toEqual(['the quick', 'brown fox']);
    expect(StringHelper.toChunks('the quick brown fox?', 10)).toEqual(['the quick', 'brown fox?']);
  });
});
