import {ValueProvider} from '@angular/core';
import {HELPER_CONFIG, HelperConfig} from './config';
import {loggerOf} from './logger/logger';

export const provideDfxHelper = (configuration: HelperConfig = {}): ValueProvider => {
  loggerOf('provideDfxHelper').info('setup', 'Configuration file', configuration);
  return {provide: HELPER_CONFIG, useValue: configuration};
};
