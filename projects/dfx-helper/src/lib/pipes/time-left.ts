import {Pipe, PipeTransform} from '@angular/core';
import {UIHelper} from '../helper/ui';
import {DateInput} from '../helper/date';

@Pipe({
  name: 'timeleft',
  standalone: true,
  pure: true,
})
export class DfxTimeLeft implements PipeTransform {
  transform(date: DateInput, start: DateInput): string {
    return UIHelper.getTimeLeft(date, start);
  }
}
