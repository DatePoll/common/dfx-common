import {Pipe, PipeTransform} from '@angular/core';
import {DateHelper, DateInput} from '../helper/date';

@Pipe({
  name: 'timespan',
  standalone: true,
  pure: true,
})
export class DfxTimeSpan implements PipeTransform {
  transform(dateNow: DateInput, dateFuture: DateInput): string {
    return DateHelper.getTimeSpan(dateNow, dateFuture);
  }
}
