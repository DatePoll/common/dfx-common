import {Pipe, PipeTransform} from '@angular/core';
import {UndefinedOrNullOr} from '../../types';
import {isEmail} from '../../helper/string/email';

@Pipe({
  name: 'isEmail',
  standalone: true,
  pure: true,
})
export class DfxIsEmail implements PipeTransform {
  transform(text: UndefinedOrNullOr<string>): boolean {
    return isEmail(text);
  }
}
