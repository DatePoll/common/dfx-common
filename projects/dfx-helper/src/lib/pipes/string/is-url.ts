import {Pipe, PipeTransform} from '@angular/core';
import {UndefinedOrNullOr} from '../../types';
import {isUrl} from '../../helper/string/url';

@Pipe({
  name: 'isUrl',
  standalone: true,
  pure: true,
})
export class DfxIsUrl implements PipeTransform {
  transform(text: UndefinedOrNullOr<string>): boolean {
    return isUrl(text);
  }
}
