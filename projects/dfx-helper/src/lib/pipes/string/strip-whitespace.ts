import {Pipe, PipeTransform} from '@angular/core';
import {UndefinedOrNullOr} from '../../types';
import {stripWhitespace} from '../../helper/string/strip-whitespace';

@Pipe({
  name: 'stripWhitespace',
  standalone: true,
  pure: true,
})
export class DfxStripWhitespace implements PipeTransform {
  transform(text: UndefinedOrNullOr<string>): string {
    return stripWhitespace(text);
  }
}
