import {Pipe, PipeTransform} from '@angular/core';
import {UndefinedOrNullOr} from '../../types';
import {cut} from '../../helper/string/cut';

@Pipe({
  name: 'cut',
  standalone: true,
  pure: true,
})
export class DfxCut implements PipeTransform {
  transform(text: UndefinedOrNullOr<string>, maxLength = 10, suffix: UndefinedOrNullOr<string> = '...'): string {
    return cut(text, maxLength, suffix);
  }
}
