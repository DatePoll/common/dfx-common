import {Pipe, PipeTransform} from '@angular/core';
import {UndefinedOrNullOr} from '../../types';
import {upperCaseFirstLetter} from '../../helper/string/transformers';

@Pipe({
  name: 'upperCaseFirstLetter',
  standalone: true,
  pure: true,
})
export class DfxUpperCaseFirstLetter implements PipeTransform {
  transform(text: UndefinedOrNullOr<string>): string {
    return upperCaseFirstLetter(text);
  }
}
