import {Pipe, PipeTransform} from '@angular/core';
import {UndefinedOrNullOr} from '../../types';
import {truncate} from '../../helper/string/truncate';

@Pipe({
  name: 'truncate',
  standalone: true,
  pure: true,
})
export class DfxTruncate implements PipeTransform {
  transform(text: UndefinedOrNullOr<string>, maxWords = 10, suffix: UndefinedOrNullOr<string> = '...'): string {
    return truncate(text, maxWords, suffix);
  }
}
