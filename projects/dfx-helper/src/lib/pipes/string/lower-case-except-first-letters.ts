import {Pipe, PipeTransform} from '@angular/core';
import {UndefinedOrNullOr} from '../../types';
import {lowerCaseExceptFirstLetters} from '../../helper/string/transformers';

@Pipe({
  name: 'lowerCaseExceptFirstLetters',
  standalone: true,
  pure: true,
})
export class DfxLowerCaseExceptFirstLetters implements PipeTransform {
  transform(text: UndefinedOrNullOr<string>): string {
    return lowerCaseExceptFirstLetters(text);
  }
}
