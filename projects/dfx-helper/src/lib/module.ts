import {ModuleWithProviders, NgModule} from '@angular/core';

import {HELPER_CONFIG, HelperConfig} from './config';
import {loggerOf} from './logger/logger';

@NgModule()
export class DfxHelperModule {
  static setup(configuration: HelperConfig = {}): ModuleWithProviders<DfxHelperModule> {
    loggerOf('DfxHelperModule').info('setup', 'Configuration file', configuration);
    return {
      ngModule: DfxHelperModule,
      providers: [
        {
          provide: HELPER_CONFIG,
          useValue: configuration,
        },
      ],
    };
  }
}
