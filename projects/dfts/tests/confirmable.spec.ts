import {Confirmable} from '../src/lib/decorators/confirmable';

describe('Confirmable', () => {
  it('should return the original method if the user confirms the action', () => {
    // mock the confirm function to return true when called
    spyOn(window, 'confirm').and.returnValue(true);

    // call the test method and expect it to return the expected value
    expect(TestClass.testMethode('hello')).toEqual('hello');
  });

  it('should return null if the user does not confirm the action', () => {
    // mock the confirm function to return false when called
    spyOn(window, 'confirm').and.returnValue(false);

    // call the test method and expect it to return null
    expect(TestClass.testMethode('hello')).toBeNull();
  });
});

class TestClass {
  @Confirmable('Are you sure?')
  static testMethode(value: any): any {
    return value;
  }
}
