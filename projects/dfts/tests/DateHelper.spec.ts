import {formatDate, formatDateWithHoursMinutesAndSeconds, timespan} from '../src/lib/helper/date';

describe('DateHelper', () => {
  it('getFormattedWithHoursMinutesAndSeconds', () => {
    let date = new Date('2020-07-20');
    date.setHours(10);
    date.setMinutes(30);
    date.setSeconds(22);
    expect(formatDateWithHoursMinutesAndSeconds(date)).toBe('2020-07-20 10:30:22');
    date = new Date('2020-01-01');
    date.setHours(1);
    date.setMinutes(1);
    date.setSeconds(1);
    expect(formatDateWithHoursMinutesAndSeconds(date)).toBe('2020-01-01 01:01:01');
    date = new Date('2020-1-1');
    date.setHours(1);
    date.setMinutes(1);
    date.setSeconds(1);
    expect(formatDateWithHoursMinutesAndSeconds(date)).toBe('2020-01-01 01:01:01');
    date = new Date('2020-9-9');
    date.setHours(1);
    date.setMinutes(1);
    date.setSeconds(1);
    expect(formatDateWithHoursMinutesAndSeconds(date)).toBe('2020-09-09 01:01:01');
    date = new Date('2020-09-09');
    date.setHours(9);
    date.setMinutes(9);
    date.setSeconds(9);
    expect(formatDateWithHoursMinutesAndSeconds(date)).toBe('2020-09-09 09:09:09');
    date = new Date('2020-12-12');
    date.setHours(12);
    date.setMinutes(12);
    date.setSeconds(12);
    expect(formatDateWithHoursMinutesAndSeconds(date)).toBe('2020-12-12 12:12:12');
  });
  it('getFormatted', () => {
    expect(formatDate(new Date('2020-07-20'))).toBe('2020-07-20');
    expect(formatDate(new Date('2020-01-01'))).toBe('2020-01-01');
    expect(formatDate(new Date('2020-12-12'))).toBe('2020-12-12');
  });
  it('getFormatted with string', () => {
    expect(formatDate('2020-07-20')).toBe('2020-07-20');
    expect(formatDate('2020-01-01')).toBe('2020-01-01');
    expect(formatDate('2020-12-12')).toBe('2020-12-12');
  });
});

describe('getTimeSpan function', () => {
  it('should return the correct time span between two dates', () => {
    const dateNow = new Date();
    const dateFuture = new Date(Date.now() + 86400 * 1000);
    const timeSpan = timespan(dateNow, dateFuture);
    expect(timeSpan).toBe('1d, 0h, 0m, 0s');
  });

  it('should return the correct time span between two dates', () => {
    const dateNow = new Date('2022-12-06T12:00:00Z');
    const dateFuture = new Date('2022-12-07T12:00:00Z');
    const result = timespan(dateNow, dateFuture);
    expect(result).toEqual('1d, 0h, 0m, 0s');
  });
});
