# dfx-common

The collection of collections.

## Subprojects

| Description        | Command             |
| ------------------ | ------------------- |
| Build all projects | `npm run all:build` |
| Test all projects  | `npm run all:test`  |
| Lint all projects  | `npm run all:lint`  |

### dfx-helper

Angular Library with tons of utility tools helping in all projects.

[![NPM](https://nodei.co/npm/dfx-helper.png)](https://npmjs.org/package/dfx-helper)

| Description | Command                  |
| ----------- | ------------------------ |
| Test        | `npm run helper:test`    |
| Build       | `npm run helper:build`   |
| Publish     | `npm run helper:publish` |

### dfx-translate

A simple translation package for Angular 4 - 12.

[![NPM](https://nodei.co/npm/dfx-translate.png)](https://npmjs.org/package/dfx-translate)

| Description | Command                     |
| ----------- | --------------------------- |
| Test        | `npm run translate:test`    |
| Build       | `npm run translate:build`   |
| Publish     | `npm run translate:publish` |

### dfx-bootstrap-table

Angular table CDK implementation for Bootstrap with filtering, sorting and pagination.

[![NPM](https://nodei.co/npm/dfx-bootstrap-table.png)](https://npmjs.org/package/dfx-bootstrap-table)

| Description | Command                    |
| ----------- | -------------------------- |
| Test        | `npm run bs:table:test`    |
| Build       | `npm run bs:table:build`   |
| Publish     | `npm run bs:table:publish` |
